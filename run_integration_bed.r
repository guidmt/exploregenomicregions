#require digest
library(rtracklayer)
library(regioneR)
library(BSgenome.Mmusculus.UCSC.mm9)
require(RColorBrewer)
library('dendextend')

###pt <- overlapPermTest(A=A, B=B, ntimes=100)
###plot(pt)

tabinfofile<-read.delim(file="sampleinfo_file_integrationbed_example.txt")

setwd("/media/data_disk/repbase_analysis/NAST_carninci/")

tab_reference<-read.table(file="Carninci_Mm3_CAGE_NAST.txt",sep="\t",stringsAsFactors=F,header=F)

db <- GRanges(tab_reference[,1], ranges = IRanges(tab_reference[,2], tab_reference[,3],names=tab_reference[,10]))

setwd("/media/data_disk/atlas_mouse_data/compendium_for_repetitiveelements_analysis")

list_file<-grep(dir(),pattern=".sort.bed",value=T,invert=T)

results_overlap<-data.frame(nrow=1:nrow(tab_reference))

vector_pvalue<-NULL
vector_zscore<-NULL
length_overlap<-NULL

for(i in list_file){

print(i)

tab2<-read.table(file=i,sep="\t",stringsAsFactors=F,header=F)

IRange.reads=GRanges(seqnames=Rle(tab2[,1]),ranges=IRanges(tab2[,2], tab2[,3]))

counts.RNAseq=data.frame(countOverlaps(db, IRange.reads))

results_overlap<-cbind(results_overlap,counts.RNAseq[,1])

pt <- overlapPermTest(A=tab_reference, B=tab2,genome="mm9",ntimes=100, verbose=TRUE,non.overlapping=FALSE)

vector_pvalue<-c(vector_pvalue,pt$pval)
vector_zscore<-c(vector_zscore,pt$zscore)
length_overlap<-c(length_overlap,pt$observed)
print(pt$observed)

}

#names_column<-i





results_overlap[,1]<-tab_reference[,10]

results_overlap<-data.frame(results_overlap[,1],tab_reference[,11],results_overlap[,c(2:ncol(results_overlap))])

colnames(results_overlap)<-c("name.rep","type.rep",list_file)

results_overlap<-data.frame(tab_reference[,c(1:3)],results_overlap)
 

setwd("/media/data_disk/repbase_analysis/NAST_carninci/integrate_NAST_TF")

write.table(results_overlap,file="NAST_overlap_ATLAS_mm9_coord.txt",sep="\t",row.names=F,quote=F,col.names=T)

nozero<- which(rowSums(results_overlap[,c(6:ncol(results_overlap))])!=0)

write.table(results_overlap[nozero,],file="NAST_overlap_ATLAS_mm9_nozero_coord.txt",sep="\t",row.names=F,quote=F,col.names=T)

nozero2<- which(rowSums(results_overlap[,c(6:ncol(results_overlap))])>2)

write.table(results_overlap[nozero2,],file="NAST_overlap_ATLAS_mm9_nozero_over2bind_coord.txt",sep="\t",row.names=F,quote=F,col.names=T)

setwd("/media/data_disk/script_utility")
save.image("run_integration.RData")


rownames(results_overlap)<-results_overlap[,5]

###
### Create the matrix with the p-value
###

#to save time I removed all regions without a overlaps (nozero)
subtab_for_pvalue<-results_overlap[,6:ncol(results_overlap)]

#i want only a binary matrix with zero and one this because during the conversion with the p-value the script can have problem to manage the substitution of gsub

subtab_for_pvalue<-subtab_for_pvalue/subtab_for_pvalue

subtab_for_pvalue[is.na(subtab_for_pvalue)]<-0

for(c in 1:ncol(subtab_for_pvalue)){
print(c)
select_pvalue<-vector_pvalue[c]
	#change the presence with the p-values
	subtab_for_pvalue[,c]<-gsub(subtab_for_pvalue[,c],pattern="1",replacement=select_pvalue,fixed=T)
	subtab_for_pvalue[,c]<-as.numeric(subtab_for_pvalue[,c])	
}


matrixpvalue<-as.matrix(subtab_for_pvalue)

colnames(matrixpvalue)<-tabinfofile[,2]

###
### In this step i perform the clustering on the rows and columns
###

d_column <- dist(t(matrixpvalue), method = "euclidean") # distance matrix
fit_column <- hclust(d_column, method="centroid")

##d_row<-dist(matrixpvalue,method="euclidean")
##fit_row<-hclust(d_row,method="centroid")

##k depends on the number of branch obsrved a priori
pdf("dendrogram_pvalue.pdf",width=30,height=10)

	par(mar=c(30,5,5,5))

	d2=color_branches(fit_column,k=30)

	plot(d2)

dev.off()


###
### Correlation heatmap
###
n<-ncol(results_overlap[nozero,c(6:ncol(results_overlap))])

tab_for_correlation<-results_overlap[nozero,c(6:ncol(results_overlap))]
colnames(tab_for_correlation)<-tabinfofile[,2]

cor_results_overlap<-cor(tab_for_correlation,method="pearson")

cor_results_overlap[lower.tri (cor_results_overlap)] <- NA

color<-rev(brewer.pal(11, "Spectral"))
pairs.breaks<-c(-1,-0.50,-0.30,-0.10,-0.05,0,0.05,0.10,0.30,0.5,0.7,1)

pdf("correlation_heatmap.pdf",width=20,height=20)
heatmap.2(cor_results_overlap, col=color,breaks=pairs.breaks, Rowv = NA, Colv = NA,scale="none",keysize = 0.5,density.info="none", trace="none",symkey=FALSE,cexRow=0.5,cexCol=0.5,margins =c(20,20))
dev.off()



###
### P-values Correlation heatmap
###

cor_results_overlap<-cor(matrixpvalue,method="pearson")

#diag(cor_results_overlap) <- NA
cor_results_overlap[lower.tri (cor_results_overlap)] <- NA


color<-rev(brewer.pal(11, "Spectral"))
pairs.breaks<-c(-1,-0.50,-0.30,-0.10,-0.05,0,0.05,0.10,0.30,0.5,0.7,1)

pdf("pvalues_correlation_heatmap.pdf",width=20,height=20)
heatmap.2(cor_results_overlap, col=color,breaks=pairs.breaks, Rowv = NA, Colv = NA,scale="none",keysize = 0.5,density.info="none", trace="none",symkey=FALSE,cexRow=0.5,cexCol=0.5,margins =c(20,20))
dev.off()


####
####
####

#get the distribution of p-values
quantileHeight_column<-data.frame(quantile(fit_column$height))
#quantileHeight_row<-data.frame(quantile(fit_row$height))


#create a matrix for the stratification of p-values, useful during clustering
extractQuantile<-function(quantileHeight){

dfclust<-data.frame()

for(c in 1:nrow(quantileHeight)){

	if(c+1 <=nrow(quantileHeight))

	{


	dfrow<-data.frame(quantileHeight[c,1],quantileHeight[c+1,1])
	
	dfclust<-rbind(dfclust,dfrow)

	}


}
return(dfclust)
}


#call a function to manage the hclust object and cut in specific position
#this function is usefull but the script will use it only to extract the gene name

cutHclust<-function(fit,quantileHeight){

list_save_hclust<-list(1:nrow(quantileHeight))

for(i in 1:nrow(quantileHeight)){

fit_object<-list(1:7)

subset_row<-which(fit$height>=dfclust[i,1]& fit$height<dfclust[i,2])

fit_object[[1]]<-fit$merge[subset_row,]
fit_object[[2]]<-fit$height[subset_row]
fit_object[[3]]<-fit$order[subset_row]
fit_object[[4]]<-fit$labels[subset_row]
fit_object[[5]]<-fit$method
fit_object[[6]]<-fit$call
fit_object[[7]]<-fit$dist.method

class(fit_object)<-"hclust"

list_save_hclust[[i]]<-fit_object

}

return(list_save_hclust)

}




#df_quantile_height_row<-extractQuantile(quantileHeight_row)
df_quantile_height_column<-extractQuantile(quantileHeight_column)
fit_hclust_cut<-cutHclust(fit_column,df_quantile_height_column)

##
## Then I used a function that cut the tree where i want
##

##
## TO DO: ADD loop for to select genes obtained after select
##
pdf("test.pdf",width=10,height=10)

color<-colorRampPalette(c("blue4","red","red2","red3","red4","firebrick4"))(n=33)

pairs.breaks<-c(0.15,seq(0.05,0.04,length.out=8),seq(0.04,0.01,length.out=8),seq(0.009,0.005,length.out=8),seq(0.005,0.001,length.out=8),0.0009)

pairs.breaks<- pairs.breaks[order(pairs.breaks)]

sub_fit<-fit_hclust_cut[[1]]

class(sub_fit)<-"hclust"

##set colors for the heatmap
labels<-unique(results_overlap[,5])
hmcols <- rainbow(length(labels));

df_colors<-data.frame(labels,hmcols)

association_colors<-merge(results_overlap,df_colors,by.x="type.rep",by.y="labels")
association_colors<-association_colors$hmcols

##here i used the results of the clustering for row and  columns using the default parameters of heatmap2 but i used the genes found clusterized together before
heatmap.2(t(matrixpvalue[,sub_fit[[4]]]),col=color,dendrogram="row",scale="none",keysize = 0.5,density.info="none", trace="none",symkey=FALSE,cexRow=0.5,cexCol=0.5,margins =c(10,10),ColSideColors=t(data.frame(association_colors)))
dev.off()




